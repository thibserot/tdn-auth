import { Component, OnInit } from '@angular/core';
import {FormControl, Validators, FormBuilder, FormGroup} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from './auth.service';

@Component({
    selector: 'auth-reset-password-confirm',
    template: `
<form [formGroup]="resetPasswordConfirmForm" (ngSubmit)="resetPassword()" fxLayout="column" fxLayoutAlign="space-around center">
    <mat-card>
        <mat-card-header>
            <mat-card-title>Change my password</mat-card-title>
        </mat-card-header>
        <mat-card-content fxLayout="column">
            <mat-form-field>
                <input matInput placeholder="New Password" type="password" formControlName="new_password1"/>
                <mat-error *ngIf="resetPasswordConfirmForm.controls.new_password1.hasError('required')">
                    Password is <strong>required</strong>
                </mat-error>
                <mat-error *ngIf="resetPasswordConfirmForm.controls.new_password1.hasError('asyncerror')">
                    {{errorNewPassword1}}
                </mat-error>
            </mat-form-field>
            <mat-form-field>
                <input matInput placeholder="New Password (Again)" type="password" formControlName="new_password2"/>
                <mat-error *ngIf="resetPasswordConfirmForm.controls.new_password2.hasError('required')">
                    Password is <strong>required</strong>
                </mat-error>
                <mat-error *ngIf="resetPasswordConfirmForm.controls.new_password2.hasError('asyncerror')">
                    {{errorNewPassword2}}
                </mat-error>
            </mat-form-field>
            <ng2-password-strength-bar
                [passwordToCheck]="resetPasswordConfirmForm.controls.new_password1.value"
                barLabel="Password Strength:">
            </ng2-password-strength-bar>
 
            <mat-error *ngIf="errorGeneral">
                {{errorGeneral}}
            </mat-error>
            <div class="success" *ngIf="success">
                {{success}}
            </div>
        </mat-card-content>
        <mat-card-actions>
            <button mat-button color="primary" type="submit" fxFlex>
                Reset Password
            </button>
        </mat-card-actions>
    </mat-card>
</form>
    `,
    styles: [``]
})
export class ResetPasswordConfirmComponent implements OnInit {
    resetPasswordConfirmForm: FormGroup;
    errorNewPassword1: string;
    errorNewPassword2: string;
    errorGeneral: string;
    success: string;

    constructor(private authService: AuthService, private formBuilder: FormBuilder, private route: ActivatedRoute) {}

    ngOnInit() {
        this.resetPasswordConfirmForm = this.formBuilder.group({
            new_password1: [null, Validators.required],
            new_password2: [null, Validators.required],
        });
    }

    resetPassword() {
        let token:string = <string>(this.route.snapshot.paramMap.get('token'));
        let uid:string = <string>(this.route.snapshot.paramMap.get('uid'));
                if (! this.resetPasswordConfirmForm.valid) {
            return;
        }
        let new_password1 = this.resetPasswordConfirmForm.controls.new_password1.value;
        let new_password2 = this.resetPasswordConfirmForm.controls.new_password2.value;
        this.authService.resetPasswordConfirm(uid, token, new_password1, new_password2).then(rsp => {
            this.errorNewPassword1 = "";
            this.errorNewPassword2 = "";
            this.errorGeneral = "";
            this.success = "";
            console.log(rsp);
            if ("detail" in rsp) {
                this.success = rsp.detail;
            }
        }).catch(error => {
            this.errorNewPassword1 = "";
            this.errorNewPassword2 = "";
            this.errorGeneral = "";
            this.success = "";
            if ("error" in error) {

                if ("new_password1" in error.error) {
                    for (let err of error.error.new_password1) {
                        this.errorNewPassword1 += err + "\n";
                    }
                    this.resetPasswordConfirmForm.controls.new_password1.setErrors({'asyncerror': true});
                }

                if ("new_password2" in error.error) {
                    for (let err of error.error.new_password2) {
                        this.errorNewPassword2 += err + "\n";
                    }
                    this.resetPasswordConfirmForm.controls.new_password2.setErrors({'asyncerror': true});
                }

                if ("non_field_errors" in error.error) {
                    for (let err of error.error.non_field_errors) {
                        this.errorGeneral += err + "\n";
                    }
                }
            }
            if ("message" in error) {
                if (this.errorGeneral === "" && this.errorNewPassword1 === "" && this.errorNewPassword2 === "") {
                    this.errorGeneral = "The token you are using is invalid or expired";
                }
            }
        });


    }

}
