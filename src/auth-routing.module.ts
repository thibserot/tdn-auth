import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './login.component';
import { RegisterComponent } from './register.component';
import { ConfirmEmailComponent } from './confirm-email.component';
import { LogoutComponent } from './logout.component';
import { ProfileComponent } from './profile.component';
import { ChangePasswordComponent } from './change-password.component';
import { ResetPasswordComponent } from './reset-password.component';
import { ResetPasswordConfirmComponent } from './reset-password-confirm.component';
import { AuthGuard } from './auth-guard.service';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'logout', component: LogoutComponent , canActivate: [AuthGuard]},
  { path: 'register', component: RegisterComponent },
  { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },
  { path: 'confirm-email/:key', component: ConfirmEmailComponent },
  { path: 'change-password', component: ChangePasswordComponent , canActivate: [AuthGuard]},
  { path: 'reset-password/confirm/:uid/:token', component: ResetPasswordConfirmComponent },
  { path: 'reset-password', component: ResetPasswordComponent },
];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ]
})
export class AuthRoutingModule { }
