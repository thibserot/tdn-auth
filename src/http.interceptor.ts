import {Injectable} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {HttpEvent, HttpInterceptor, HttpHandler, HttpRequest} from '@angular/common/http';
import { CredentialsStorageService } from './credentials-storage.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    constructor(private credentialStorage: CredentialsStorageService) {}
    
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // Get the auth header from the service.
        //const authHeader = this.auth.getAuthorizationHeader();
        // Clone the request to add the new header.
        const credentials = this.credentialStorage.get();
        const authReq = req.clone({headers: req.headers.set('Authorization', credentials)});

        //const authReq = req.clone(); //{headers: req.headers.set('Access-Control-Allow-Origin', '*')});
        // Pass on the cloned request instead of the original request.
        //console.log("Intercepting", authReq);
        return next.handle(authReq);
    }
}
