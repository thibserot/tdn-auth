import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from './auth.service';

@Component({
  selector: 'auth-confirm-email',
  template: `
<div fxLayout="column" fxLayoutAlign="space-around center">
    <mat-card>
        <mat-card-content fxLayout="column">

            <p *ngIf="hasError" class="center">
                The key you entered is invalid.<br/>
                Please double check or ask for your activation key to be re-sent.
            </p>
            <p *ngIf="!hasError" class="center">
                Your account has been succesfully activated.<br/>
                You can now login to access to the full website functionalities.
            </p>
        </mat-card-content>
        <mat-card-actions>
            <button mat-button color="primary" type="button" routerLink="/login" fxFlex *ngIf="!hasError">
                Login
            </button>
            <button mat-button color="primary" type="button" routerLink="/resend-confirm-email" fxFlex *ngIf="hasError">
                Send Confirmation email again
            </button>
        </mat-card-actions>
    </mat-card>
</div>
  `,
  styles: [`
.center {
    text-align: center;
}`]
})
export class ConfirmEmailComponent implements OnInit {
    hasError: boolean = true;

    constructor(private authService: AuthService, private route: ActivatedRoute) { }

    ngOnInit() {
        let key:string|null = this.route.snapshot.paramMap.get('key');
        this.authService.confirmEmail(<string>(key)).then(rsp => {
            this.hasError = false;

        }).catch(error => {
            this.hasError = true;
        });

    }
}
