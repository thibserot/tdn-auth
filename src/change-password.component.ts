import { Component, OnInit } from '@angular/core';
import {FormControl, Validators, FormBuilder, FormGroup} from '@angular/forms';
import { AuthService } from './auth.service';

@Component({
    selector: 'auth-change-password',
    template: `
<form [formGroup]="changePasswordForm" (ngSubmit)="changePassword()" fxLayout="column" fxLayoutAlign="space-around center">
    <mat-card>
        <mat-card-header>
            <mat-card-title>Change my password</mat-card-title>
        </mat-card-header>
        <mat-card-content fxLayout="column">
            <mat-form-field>
                <input matInput placeholder="Old Password" type="password" formControlName="old_password"/>
                <mat-error *ngIf="changePasswordForm.controls.old_password.hasError('required')">
                    Password is <strong>required</strong>
                </mat-error>
                <mat-error *ngIf="changePasswordForm.controls.old_password.hasError('asyncerror')">
                    {{errorOldPassword}}
                </mat-error>
            </mat-form-field>
            <mat-form-field>
                <input matInput placeholder="New Password" type="password" formControlName="new_password1"/>
                <mat-error *ngIf="changePasswordForm.controls.new_password1.hasError('required')">
                    Password is <strong>required</strong>
                </mat-error>
                <mat-error *ngIf="changePasswordForm.controls.new_password1.hasError('asyncerror')">
                    {{errorNewPassword1}}
                </mat-error>
            </mat-form-field>
            <mat-form-field>
                <input matInput placeholder="New Password (Again)" type="password" formControlName="new_password2"/>
                <mat-error *ngIf="changePasswordForm.controls.new_password2.hasError('required')">
                    Password is <strong>required</strong>
                </mat-error>
                <mat-error *ngIf="changePasswordForm.controls.new_password2.hasError('asyncerror')">
                    {{errorNewPassword2}}
                </mat-error>
            </mat-form-field>
            <ng2-password-strength-bar
                [passwordToCheck]="changePasswordForm.controls.new_password1.value"
                barLabel="Password Strength:">
            </ng2-password-strength-bar>
 
            <mat-error *ngIf="errorGeneral">
                {{errorGeneral}}
            </mat-error>
            <div class="success" *ngIf="success">
                {{success}}
            </div>
        </mat-card-content>
        <mat-card-actions>
            <button mat-button color="primary" type="submit" fxFlex>
                Change Password
            </button>
        </mat-card-actions>
    </mat-card>
</form>
    `,
    styles: [``]
})
export class ChangePasswordComponent implements OnInit {
    changePasswordForm: FormGroup;
    errorNewPassword1: string;
    errorNewPassword2: string;
    errorOldPassword: string;
    errorGeneral: string;
    success: string;


    constructor(private authService: AuthService, private formBuilder: FormBuilder) { }

    ngOnInit() {
        this.changePasswordForm = this.formBuilder.group({
            old_password: [null, Validators.required],
            new_password1: [null, Validators.required],
            new_password2: [null, Validators.required],
        });
    }

    changePassword() {
        if (! this.changePasswordForm.valid) {
            return;
        }
        let new_password1 = this.changePasswordForm.controls.new_password1.value;
        let new_password2 = this.changePasswordForm.controls.new_password2.value;
        let old_password = this.changePasswordForm.controls.old_password.value;
        this.authService.changePassword(old_password, new_password1, new_password2).then(rsp => {
            this.errorNewPassword1 = "";
            this.errorNewPassword2 = "";
            this.errorOldPassword = "";
            this.errorGeneral = "";
            this.success = "";
            console.log(rsp);
            if ("detail" in rsp) {
                this.success = rsp.detail;
            }
        }).catch(error => {
            this.errorNewPassword1 = "";
            this.errorNewPassword2 = "";
            this.errorOldPassword = "";
            this.errorGeneral = "";
            this.success = "";
            if ("error" in error) {

                if ("new_password1" in error.error) {
                    for (let err of error.error.new_password1) {
                        this.errorNewPassword1 += err + "\n";
                    }
                    this.changePasswordForm.controls.new_password1.setErrors({'asyncerror': true});
                }

                if ("new_password2" in error.error) {
                    for (let err of error.error.new_password2) {
                        this.errorNewPassword2 += err + "\n";
                    }
                    this.changePasswordForm.controls.new_password2.setErrors({'asyncerror': true});
                }

                if ("old_password" in error.error) {
                    for (let err of error.error.old_password) {
                        this.errorOldPassword += err + "\n";
                    }
                    this.changePasswordForm.controls.old_password.setErrors({'asyncerror': true});
                }
                if ("non_field_errors" in error.error) {
                    for (let err of error.error.non_field_errors) {
                        this.errorGeneral += err + "\n";
                    }
                }
            }
            if ("message" in error) {
                if (this.errorGeneral === "" && this.errorNewPassword1 === "" && this.errorNewPassword2 === "" && this.errorOldPassword === "") {
                    this.errorGeneral += error.message + "\n";
                }
            }
        });
    }


}
