import { Component, OnInit, Input } from '@angular/core';
import {FormControl, Validators, FormBuilder, FormGroup} from '@angular/forms';

import { AuthService } from './auth.service';

@Component({
  selector: 'auth-register',
  template: `
<form [formGroup]="registerForm" (ngSubmit)="register()" fxLayout="column" fxLayoutAlign="space-around center">
    <mat-card>
        <mat-card-content fxLayout="column">
            <mat-form-field>
                <input matInput placeholder="Username" formControlName="username"/>
                <mat-error *ngIf="registerForm.controls.username.hasError('required')">
                    Username is <strong>required</strong>
                </mat-error>
                <mat-error *ngIf="registerForm.controls.username.hasError('asyncerror')">
                    {{errorUsername}}
                </mat-error>
            </mat-form-field>
            <mat-form-field>
                <input matInput placeholder="Email" formControlName="email"/>
                <mat-error *ngIf="registerForm.controls.email.hasError('required')">
                    Email is <strong>required</strong>
                </mat-error>
                <mat-error *ngIf="registerForm.controls.email.hasError('asyncerror')">
                    {{errorEmail}}
                </mat-error>
            </mat-form-field>
            <mat-form-field>
                <input matInput placeholder="Password" type="password" formControlName="password1"/>
                <mat-error *ngIf="registerForm.controls.password1.hasError('required')">
                    Password is <strong>required</strong>
                </mat-error>
                <mat-error *ngIf="registerForm.controls.password1.hasError('asyncerror')">
                    {{errorPassword1}}
                </mat-error>
            </mat-form-field>
            <mat-form-field>
                <input matInput placeholder="Password (Again)" type="password" formControlName="password2"/>
                <mat-error *ngIf="registerForm.controls.password2.hasError('required')">
                    Password is <strong>required</strong>
                </mat-error>
                <mat-error *ngIf="registerForm.controls.password2.hasError('asyncerror')">
                    {{errorPassword2}}
                </mat-error>
            </mat-form-field>
            <ng2-password-strength-bar
                [passwordToCheck]="registerForm.controls.password1.value"
                barLabel="Password Strength:">
            </ng2-password-strength-bar>
 
            <mat-error *ngIf="errorGeneral">
                {{errorGeneral}}
            </mat-error>
            <div class="success" *ngIf="success">
                {{success}}
            </div>
        </mat-card-content>
        <mat-card-actions>
            <button mat-button color="primary" type="submit" fxFlex>
                Register
            </button>
        </mat-card-actions>
    </mat-card>
</form>
  `,
  styles: [``]
})
export class RegisterComponent implements OnInit {
    registerForm: FormGroup;
    errorGeneral: string;
    errorPassword1: string;
    errorPassword2: string;
    errorUsername: string;
    errorEmail: string;
    success: string;

    constructor(private authService: AuthService, private formBuilder: FormBuilder) {}

    ngOnInit() {
        this.registerForm = this.formBuilder.group({
            username: [null, Validators.required],
            password1: [null, Validators.required],
            password2: [null, Validators.required],
            email: [null, Validators.required]
        });
    }

    register() {
        if (! this.registerForm.valid) {
            return;
        }
        let username = this.registerForm.controls.username.value;
        let email = this.registerForm.controls.email.value;
        let password1 = this.registerForm.controls.password1.value;
        let password2 = this.registerForm.controls.password2.value;
        this.authService.register(username, email, password1, password2).then(rsp => {
            console.log(rsp);
            this.errorPassword1 = "";
            this.errorPassword2 = "";
            this.errorUsername = "";
            this.errorEmail = "";
            this.errorGeneral = "";
            this.success = rsp.detail;
        }).catch(error => {
            this.errorPassword1 = "";
            this.errorPassword2 = "";
            this.errorUsername = "";
            this.errorEmail = "";
            this.errorGeneral = "";
            if ("error" in error) {

                if ("password1" in error.error) {
                    for (let err of error.error.password1) {
                        this.errorPassword1 += err + "\n";
                    }
                    this.registerForm.controls.password1.setErrors({'asyncerror': true});
                }
                if ("password2" in error.error) {
                    for (let err of error.error.password2) {
                        this.errorPassword2 += err + "\n";
                    }
                    this.registerForm.controls.password2.setErrors({'asyncerror': true});
                }
                if ("username" in error.error) {
                    for (let err of error.error.username) {
                        this.errorUsername += err + "\n";
                    }
                    this.registerForm.controls.username.setErrors({'asyncerror': true});
                }
                if ("email" in error.error) {
                    for (let err of error.error.email) {
                        this.errorEmail += err + "\n";
                    }
                    this.registerForm.controls.email.setErrors({'asyncerror': true});
                }
                if ("non_field_errors" in error.error) {
                    for (let err of error.error.non_field_errors) {
                        this.errorGeneral += err + "\n";
                    }
                }
            }
            if ("message" in error) {
                if (this.errorGeneral === "" && this.errorPassword1 === "" && this.errorPassword2 === "" && this.errorUsername === "" && this.errorEmail === "") {
                    this.errorGeneral = error.message + "\n";

                }
            }
        });
    }



}
