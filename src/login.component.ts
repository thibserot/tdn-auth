import { Component, OnInit, Input } from '@angular/core';
import {FormControl, Validators, FormBuilder, FormGroup} from '@angular/forms';
import { Router }      from '@angular/router';

import { AuthService } from './auth.service';

@Component({
  selector: 'auth-login',
  template: `
<form [formGroup]="loginForm" (ngSubmit)="login()" fxLayout="column" fxLayoutAlign="space-around center">
    <mat-card>
        <mat-card-content fxLayout="column">
            <mat-form-field>
                <input matInput placeholder="Username" formControlName="username"/>
                <mat-error *ngIf="loginForm.controls.username.hasError('required')">
                    Username is <strong>required</strong>
                </mat-error>
                <mat-error *ngIf="loginForm.controls.username.hasError('asyncerror')">
                    {{errorUsername}}
                </mat-error>
            </mat-form-field>
            <mat-form-field>
                <input matInput placeholder="Password" type="password" formControlName="password"/>
                <mat-error *ngIf="loginForm.controls.password.hasError('required')">
                    Password is <strong>required</strong>
                </mat-error>
                <mat-error *ngIf="loginForm.controls.password.hasError('asyncerror')">
                    {{errorPassword}}
                </mat-error>
            </mat-form-field>
            <mat-error *ngIf="errorGeneral">
                {{errorGeneral}}
            </mat-error>
        </mat-card-content>
        <mat-card-actions>
            <button mat-button color="primary" type="submit" fxFlex>
                Login
            </button>
            <button mat-button color="primary" type="button" routerLink="/register" fxFlex>
                Register
            </button>
        </mat-card-actions>
    </mat-card>
</form>
  `,
  styles: [``]
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    errorGeneral: string;
    errorPassword: string;
    errorUsername: string;

    constructor(private authService: AuthService, private formBuilder: FormBuilder, public router: Router) {}

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            username: [null, Validators.required],
            password: [null, Validators.required]
        });
    }

    login() {
        if (! this.loginForm.valid) {
            return;
        }
        let username = this.loginForm.controls.username.value;
        let password = this.loginForm.controls.password.value;
        this.authService.login(username, password).then(rsp => {
            this.errorPassword = "";
            this.errorUsername = "";
            this.errorGeneral = "";
            this.router.navigate([this.authService.redirect()]);
        }).catch(error => {
            this.errorPassword = "";
            this.errorUsername = "";
            this.errorGeneral = "";
            if ("error" in error) {

                if ("password" in error.error) {
                    for (let err of error.error.password) {
                        this.errorPassword += err + "\n";
                    }
                    this.loginForm.controls.password.setErrors({'asyncerror': true});
                }
                if ("username" in error.error) {
                    for (let err of error.error.username) {
                        this.errorUsername += err + "\n";
                    }
                    this.loginForm.controls.username.setErrors({'asyncerror': true});
                }
                if ("non_field_errors" in error.error) {
                    for (let err of error.error.non_field_errors) {
                        this.errorGeneral += err + "\n";
                    }
                }
            }
            if ("message" in error) {
                if (this.errorGeneral === "" && this.errorPassword === "" && this.errorUsername === "") {
                this.errorGeneral += error.message + "\n";

            }
            }
        });
    }

}
