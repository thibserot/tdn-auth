import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie';

@Injectable()
export class CredentialsStorageService {
    token: string = "";
    constructor(private cookieService: CookieService) { 
        this.token = this.cookieService.get("Token");
    }

    get(): string {
        let header: string = "";
        if (this.token !== "" && this.token !== undefined) {
            header = "Token " + this.token;
        }
        return header;
    }

    set(token:string) {
        if (token === "") {
            this.token = "";
            this.cookieService.remove("Token");

        } else {
            this.token = token;
            this.cookieService.put("Token", token);
        }
    }

}
