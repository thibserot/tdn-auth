import { Component, OnInit } from '@angular/core';
import { AuthService } from './auth.service';

@Component({
    selector: 'auth-logout',
    template: `
<div fxLayout="column" fxLayoutAlign="space-around center">
    <mat-card>
        <mat-card-content>
            You have been logged out.
        </mat-card-content>
        <mat-card-actions>
            <button mat-button color="primary" type="button" routerLink="/login" fxFlex>
                Login
            </button>
        </mat-card-actions>
    </mat-card>
</div>
    `,
    styles: [``]
})
export class LogoutComponent implements OnInit {

    constructor(private authService: AuthService) { }

    ngOnInit() {
        this.authService.logout().then(rsp => {
            console.log(rsp);

        }).catch(error => {
            console.error(error);
        });

    }
}
