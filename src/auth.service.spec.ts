import { TestBed, inject } from '@angular/core/testing';

import { AuthService } from './auth.service';
import { AuthConfig } from './auth.config';

describe('AuthService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthService, AuthConfig]
    });
  });

  //it('should be created', inject([AuthService, AuthConfig], (service: AuthService) => {
  //  expect(service).toBeTruthy();
  //}));
});
