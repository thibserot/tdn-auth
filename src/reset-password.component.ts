import { Component, OnInit } from '@angular/core';
import {FormControl, Validators, FormBuilder, FormGroup} from '@angular/forms';
import { AuthService } from './auth.service';


@Component({
    selector: 'auth-reset-password',
    template: `
<form [formGroup]="resetPasswordForm" (ngSubmit)="resetPassword()" fxLayout="column" fxLayoutAlign="space-around center">
    <mat-card>
        <mat-card-header>
            <mat-card-title>Reset my password</mat-card-title>
        </mat-card-header>
        <mat-card-content fxLayout="column">
            <mat-form-field>
                <input matInput placeholder="Email" formControlName="email" type="email"/>
                <mat-error *ngIf="resetPasswordForm.controls.email.hasError('required')">
                    Email is <strong>required</strong>
                </mat-error>
                <mat-error *ngIf="resetPasswordForm.controls.email.hasError('asyncerror')">
                    {{errorEmail}}
                </mat-error>
            </mat-form-field>
            <mat-error *ngIf="errorGeneral">
                {{errorGeneral}}
            </mat-error>
            <div class="success" *ngIf="success">
                {{success}}
            </div>
        </mat-card-content>
        <mat-card-actions>
            <button mat-button color="primary" type="submit" fxFlex>
                Reset Password
            </button>
        </mat-card-actions>
    </mat-card>
</form>
    `,
    styles: [``]
})
export class ResetPasswordComponent implements OnInit {
    resetPasswordForm: FormGroup;
    errorEmail: string;
    errorGeneral: string;
    success: string;

    constructor(private authService: AuthService, private formBuilder: FormBuilder) { }

    ngOnInit() {
        this.resetPasswordForm = this.formBuilder.group({
            email: [null, Validators.required],
        });
    }

    resetPassword() {
        if (! this.resetPasswordForm.valid) {
            return;
        }
        let email = this.resetPasswordForm.controls.email.value;
        this.authService.resetPassword(email).then(rsp => {
            this.errorEmail = "";
            this.errorGeneral = "";
            this.success = "";
            console.log(rsp);
            if ("detail" in rsp) {
                this.success = rsp.detail;
            }
        }).catch(error => {
            this.errorEmail = "";
            this.errorGeneral = "";
            this.success = "";
            if ("error" in error) {

                if ("email" in error.error) {
                    for (let err of error.error.email) {
                        this.errorEmail += err + "\n";
                    }
                    this.resetPasswordForm.controls.email.setErrors({'asyncerror': true});
                }

                if ("non_field_errors" in error.error) {
                    for (let err of error.error.non_field_errors) {
                        this.errorGeneral += err + "\n";
                    }
                }
            }
            if ("message" in error) {
                if (this.errorGeneral === "" && this.errorEmail === "") {
                    this.errorGeneral += error.message + "\n";
                }
            }
        });
    }


}
