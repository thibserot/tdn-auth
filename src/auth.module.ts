import { NgModule, ModuleWithProviders} from '@angular/core';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpClientModule }    from '@angular/common/http';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';

import {MatButtonModule, MatInputModule, MatCardModule} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CookieModule } from 'ngx-cookie';

import { AuthConfig } from './auth.config';
import { AuthService } from './auth.service';
import { AuthInterceptor } from './http.interceptor';
import { LoginComponent } from './login.component';
import { AuthRoutingModule } from './auth-routing.module';
import { RegisterComponent } from './register.component';
import { ConfirmEmailComponent } from './confirm-email.component';
import { LogoutComponent } from './logout.component';
import { ProfileComponent } from './profile.component';
import { CredentialsStorageService } from './credentials-storage.service';
import { ChangePasswordComponent } from './change-password.component';
import { ResetPasswordComponent } from './reset-password.component';
import { ResetPasswordConfirmComponent } from './reset-password-confirm.component';
import { PasswordStrengthBarComponent } from './password-strength-bar.component';
import { AuthGuard } from './auth-guard.service';

@NgModule({
    imports: [
        HttpClientModule,
        CommonModule,
        FormsModule,
        CookieModule.forChild(),
        ReactiveFormsModule,
        MatButtonModule,
        MatInputModule,
        MatCardModule,
        FlexLayoutModule,
        AuthRoutingModule
        
    ],
    declarations: [LoginComponent, RegisterComponent, ConfirmEmailComponent, LogoutComponent, ProfileComponent, ChangePasswordComponent, ResetPasswordComponent, ResetPasswordConfirmComponent, PasswordStrengthBarComponent ],
    exports: [LoginComponent, RegisterComponent, ConfirmEmailComponent, LogoutComponent, ProfileComponent, ChangePasswordComponent, ResetPasswordComponent, ResetPasswordConfirmComponent, PasswordStrengthBarComponent ],
    providers: [CredentialsStorageService, AuthGuard]
})

export class AuthModule {
    static forRoot(config: any = AuthConfig): ModuleWithProviders {
        //console.log(config);
        return {
            ngModule: AuthModule,
            providers: [
                {
                    provide: HTTP_INTERCEPTORS,
                    useClass: AuthInterceptor,
                    multi: true,
                },
                AuthService,
                { 
                    provide: 'AuthConfig', 
                    useClass: config 
                }
            ]

        }
    }


}
