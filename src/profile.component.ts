import { Component, OnInit } from '@angular/core';
import {FormControl, Validators, FormBuilder, FormGroup} from '@angular/forms';
import { AuthService } from './auth.service';
import { User } from './user';

@Component({
    selector: 'auth-profile',
    template: `
<form [formGroup]="profileForm" (ngSubmit)="update()" fxLayout="column" fxLayoutAlign="space-around center">
    <mat-card>
        <mat-card-header>
            <mat-card-title>My profile</mat-card-title>
        </mat-card-header>
        <mat-card-content fxLayout="column">
            <mat-form-field>
                <input matInput placeholder="Username" formControlName="username" [(ngModel)]="user.username"/>
                <mat-error *ngIf="profileForm.controls.username.hasError('required')">
                    Username is <strong>required</strong>
                </mat-error>
                <mat-error *ngIf="profileForm.controls.username.hasError('asyncerror')">
                    {{errorUsername}}
                </mat-error>
            </mat-form-field>
            <mat-form-field>
                <input matInput placeholder="Email" formControlName="email" [(ngModel)]="user.email"/>
                <mat-error *ngIf="profileForm.controls.email.hasError('required')">
                    Email is <strong>required</strong>
                </mat-error>
                <mat-error *ngIf="profileForm.controls.email.hasError('asyncerror')">
                    {{errorEmail}}
                </mat-error>
            </mat-form-field>
            <mat-form-field>
                <input matInput placeholder="Firstname" formControlName="firstname" [(ngModel)]="user.first_name"/>
                <mat-error *ngIf="profileForm.controls.firstname.hasError('asyncerror')">
                    {{errorFirstname}}
                </mat-error>
            </mat-form-field>
            <mat-form-field>
                <input matInput placeholder="Lastname" formControlName="lastname" [(ngModel)]="user.last_name"/>
                <mat-error *ngIf="profileForm.controls.lastname.hasError('asyncerror')">
                    {{errorLastname}}
                </mat-error>
            </mat-form-field>
 
            <mat-error *ngIf="errorGeneral">
                {{errorGeneral}}
            </mat-error>
            <div class="success" *ngIf="success">
                {{success}}
            </div>
        </mat-card-content>
        <mat-card-actions>
            <button mat-button color="primary" type="submit">
                Save
            </button>
            <button mat-button color="primary" type="button" routerLink="/change-password">
                Change password
            </button>
        </mat-card-actions>
    </mat-card>
</form>
    `,
    styles: [``]
})
export class ProfileComponent implements OnInit {
    profileForm: FormGroup;
    user: User = new User();
    errorUsername: string;
    errorFirstname: string;
    errorLastname: string;
    errorEmail: string;
    errorGeneral: string;
    success: string;

    constructor(private authService: AuthService, private formBuilder: FormBuilder) { }

    ngOnInit() {
        this.user = new User();
        this.profileForm = this.formBuilder.group({
            username: [null, Validators.required],
            firstname: [null, ],
            lastname: [null, ],
            email: [ {value: '', disabled: true}, ]
        });

        this.errorUsername = "Test";
        this.authService.getProfile().then(rsp => {
            this.user = rsp;

        }).catch(error => {
            this.user = new User();
            console.error(error);
        });

    }

    update() {
        if (! this.profileForm.valid) {
            return;
        }
        this.authService.updateProfile(<User>(this.user)).then(rsp => {
            this.errorUsername = "";
            this.errorFirstname = "";
            this.errorLastname = "";
            this.errorEmail = "";
            this.errorGeneral = "";
            this.success = "Profile updated";
        }).catch(error => {
            this.errorUsername = "";
            this.errorFirstname = "";
            this.errorLastname = "";
            this.errorEmail = "";
            this.errorGeneral = "";
            this.success = "";
            if ("error" in error) {

                if ("username" in error.error) {
                    for (let err of error.error.username) {
                        this.errorUsername += err + "\n";
                    }
                    this.profileForm.controls.username.setErrors({'asyncerror': true});
                }

                if ("email" in error.error) {
                    for (let err of error.error.email) {
                        this.errorEmail += err + "\n";
                    }
                    this.profileForm.controls.email.setErrors({'asyncerror': true});
                }
                if ("first_name" in error.error) {
                    for (let err of error.error.first_name) {
                        this.errorFirstname += err + "\n";
                    }
                    this.profileForm.controls.firstname.setErrors({'asyncerror': true});
                }

                if ("last_name" in error.error) {
                    for (let err of error.error.last_name) {
                        this.errorLastname += err + "\n";
                    }
                    this.profileForm.controls.lastname.setErrors({'asyncerror': true});
                }
                if ("non_field_errors" in error.error) {
                    for (let err of error.error.non_field_errors) {
                        this.errorGeneral += err + "\n";
                    }
                }
            }
            if ("message" in error) {
                if (this.errorGeneral === "" && this.errorUsername === "" && this.errorEmail === "" && this.errorFirstname === "" && this.errorLastname === "") {
                this.errorGeneral += error.message + "\n";

            }
            }
        });
    }


}
