import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { AuthConfig } from './auth.config';
import { CredentialsStorageService } from './credentials-storage.service';
import { User } from './user';

@Injectable()
export class AuthService {
    credentials: string = "";
    user: User | undefined;
    is_authenticated: boolean = false;
    redirectUrl: string;


    constructor(@Inject('AuthConfig') private config: AuthConfig, private http: HttpClient, private credentialStorage: CredentialsStorageService) { 
        console.log(config);
        this.getProfile(true).catch(error => {});
    }

    isAuthenticated(): boolean {
        return this.is_authenticated;
    }

    redirect(): string {
        let _redirect = this.redirectUrl?this.redirectUrl:this.config.getLoginRedirectUrl();
        this.redirectUrl = "";
        return _redirect;
    }

    login(username:string, password: string) : Promise<any> {
        const url = this.config.getLoginUrl();
        var payload = { 
            username : username,
            password: password
        };
        this.user = undefined;
        this.credentialStorage.set("");
        this.is_authenticated = false;

        return this.http.post(url, payload)
                    .toPromise()
                    .then(res => {
                        this.credentialStorage.set((<any>res)["key"]);
                        return this.getProfile();
                    })
                    .catch(this.handleError);

    }

    logout() : Promise<any> {
        const url = this.config.getLogoutUrl();
        var payload = { 
        };

        return this.http.post(url, payload)
                    .toPromise()
                    .then(res => {
                        this.user = undefined;
                        this.credentialStorage.set("");
                        this.is_authenticated = false;
                        return res;
                    })
                    .catch(this.handleError);

    }

    register(username:string, email: string, password1: string, password2:string) : Promise<any> {
        const url = this.config.getRegistrationUrl();
        var payload = { 
            username : username,
            email: email,
            password1: password1,
            password2: password2,
        };

        return this.http.post(url, payload)
                    .toPromise()
                    .then(res => res)
                    .catch(this.handleError);

    }

    confirmEmail(key:string) : Promise<any> {
        const url = this.config.getConfirmationEmailUrl();
        var payload = { 
            key : key,
        };

        return this.http.post(url, payload)
                    .toPromise()
                    .then(res => res)
                    .catch(this.handleError);

    }



    getProfile(force:boolean=false): Promise<User> {
        if (!force && this.user) {
            return Promise.resolve(this.user);
        }
        const url = this.config.getProfileUrl();
        return this.http.get(url)
                    .toPromise()
                    .then(res => {
                        this.user = res as User;
                        this.is_authenticated = true;

                        return this.user;
                    })
                    .catch(this.handleError);
    }

    updateProfile(user: User): Promise<User> {
        const url = this.config.getProfileUrl();
        return this.http.put(url, user)
                    .toPromise()
                    .then(res => res as User)
                    .catch(this.handleError);
    }

    changePassword(old_password:string, new_password1: string, new_password2: string): Promise<any> {
        const url = this.config.getChangePasswordUrl();
        var payload = { 
            old_password : old_password,
            new_password1 : new_password1,
            new_password2 : new_password2,
        };

        return this.http.post(url, payload)
                    .toPromise()
                    .then(res => res)
                    .catch(this.handleError);
    }

    resetPassword(email: string): Promise<any> {
        const url = this.config.getResetPasswordUrl();
        var payload = { 
            email : email,
        };

        return this.http.post(url, payload)
                    .toPromise()
                    .then(res => res)
                    .catch(this.handleError);
    }

    resetPasswordConfirm(uid:string, token:string, new_password1: string, new_password2: string): Promise<any> {
        const url = this.config.getResetPasswordConfirmUrl();
        var payload = { 
            new_password1 : new_password1,
            new_password2 : new_password2,
            uid: uid,
            token:token,
        };

        return this.http.post(url, payload)
                    .toPromise()
                    .then(res => res)
                    .catch(this.handleError);
    }



    handleError(error: any): Promise<any> {
        console.error("An error occured", error);
        return Promise.reject(error);
    }

}
