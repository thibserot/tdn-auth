import { Injectable } from '@angular/core';

@Injectable()
export class AuthConfig {
    base_url: string = "http://localhost:8000";

    login_url: string = "/rest-auth/login/";
    logout_url: string = "/rest-auth/logout/";
    registration_url: string = "/rest-auth/registration/";
    profile_url: string = "/rest-auth/user/"
    confirm_email_url: string = "/users/confirm-email/";
    change_password_url: string = "/rest-auth/password/change/"
    reset_password_url: string = "/rest-auth/password/reset/"
    reset_password_confirm_url: string = "/rest-auth/password/reset/confirm/"
    login_redirect_url: string = "/profile";

    constructor() {

    }

    getLoginUrl(): string {
        return this.base_url + this.login_url;
    }
    
    getLogoutUrl(): string {
        return this.base_url + this.logout_url;
    }

    getRegistrationUrl(): string {
        return this.base_url + this.registration_url;
    }

    getConfirmationEmailUrl(): string {
        return this.base_url + this.confirm_email_url;
    }

    getProfileUrl(): string {
        return this.base_url + this.profile_url;
    }
    
    getChangePasswordUrl(): string {
        return this.base_url + this.change_password_url;
    }

    getResetPasswordUrl(): string {
        return this.base_url + this.reset_password_url;
    }

    getResetPasswordConfirmUrl(): string {
        return this.base_url + this.reset_password_confirm_url;
    }

    getLoginRedirectUrl(): string {
        return this.login_redirect_url;
    }

}
