import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'; 

import { AppComponent } from './app.component';
import { AuthModule } from '../auth.module';
import { AuthConfig } from '../auth.config';
import { AuthService } from '../auth.service';


export class CustomAuthConfig extends AuthConfig {
    base_url: string;
    constructor() {
        super();
        this.base_url = "http://tutu";
    }
}


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AuthModule.forRoot( CustomAuthConfig )
  ],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
