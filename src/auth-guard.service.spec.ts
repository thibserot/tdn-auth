import { TestBed, inject } from '@angular/core/testing';

import { AuthGuard } from './auth-guard.service';
import { AuthService } from './auth.service';

describe('AuthGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthGuard, AuthService]
    });
  });

  //it('should be created', inject([AuthGuard, AuthService], (service: AuthGuard) => {
  //  expect(service).toBeTruthy();
  //}));
});
