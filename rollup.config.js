export default {
	input: 'dist/src/index.js',
	sourceMap: false,
    output: {
        name: 'ng.tdn-auth',
        file: 'dist/bundles/tdn-auth.umd.js',
        format: 'umd',
    },
	globals: {
		'@angular/core': 'ng.core',
		'@angular/common': 'ng.common',
		'@angular/common/http': 'ng.common.http',
        '@angular/cdk': 'ng.cdk',
        '@angular/animations': 'ng.animations',
        '@angular/flex-layout': 'ng.flex-layout',
        '@angular/forms': 'ng.forms',
        '@angular/http': 'ng.http',
        '@angular/material': 'ng.material',
        '@angular/platform-browser': 'ng.platform-browser',
        '@angular/router': 'ng.router',
        'ngx-cookie': 'ng.ngx-cookie',

		'rxjs/Observable': 'Rx',
		'rxjs/add/operator/map': 'Rx.Observable.prototype',
		'rxjs/add/operator/catch': 'Rx.Observable.prototype',
		'rxjs/add/operator/toPromise': 'Rx.Observable.prototype'
	},
    external: [
		'@angular/core',
		'@angular/common',
		'@angular/common/http',
        '@angular/cdk',
        '@angular/animations',
        '@angular/flex-layout',
        '@angular/forms',
        '@angular/http',
        '@angular/material',
        '@angular/platform-browser',
        '@angular/router',
        'ngx-cookie',

		'rxjs/Observable',
		'rxjs/add/operator/map',
		'rxjs/add/operator/catch',
		'rxjs/add/operator/toPromise'
    ]
}
