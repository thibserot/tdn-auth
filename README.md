# TDN Auth

This project provides an interface with [Django-rest-auth](http://django-rest-auth.readthedocs.io/en/latest/)

## Installation

```
npm install tdn-auth
```

## How to use it?

To use it, you must add in your app.module: 

```
import { AuthModule } from 'angular-auth/auth.module';
```

and in the imports section: 

```
AuthModule.forRoot()
```

If you need to override some parameters from the AuthModule, you can create your custom config by extending AuthConfig:
```
import { Injectable } from '@angular/core';
import { AuthConfig } from 'tdn-auth/auth.config';
import { environment } from '../environments/environment';

@Injectable()
export class CustomAuthConfig extends AuthConfig {
    constructor() {
        super();
        this.base_url = environment.api_base_url;
    }

}

```

and then inject it in the import section like this:

```
AuthModule.forRoot(CustomAuthConfig),
```

## How to create a new delivery?

Using this excellent [tutorial](https://github.com/davguij/angular-npm-module-seed), you simply need to run:

```
npm install
npm run build
```

If you want to test it locally, go in the dist folder and run `npm link`. Then go to your test app root dir and do `npm link tdn-auth`
Re-start your server with following command: `ng serve --preserve-symlinks`

## Publish to NPM

Go to the dist folder and run:
```
npm login
npm publish
```
